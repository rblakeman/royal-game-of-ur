﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flicker : MonoBehaviour {

    public float minFlicker;
    public float maxFlicker;
    public float flickerSpeed;

    private int randomizer;
    private Light light;

	// Use this for initialization
	void Start () {
        light = GetComponent<Light>();
        
        StartCoroutine(Test());

    }
	
    IEnumerator Test()
    {
        while (true)
        {
        
            light.intensity = (Random.Range(minFlicker, maxFlicker));

            yield return new WaitForSeconds (1/flickerSpeed);
        }
    }

}
